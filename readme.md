## Rød Tråd

Rød Tråd er et sosialt media hvor strikkere og bedrifter kan dele prosjekter, oppdateringer og bilder samt følge med på hva andre holder på med.

## Bakgrunn

Rød Tråd er et prosjekt som gjennomføres av studenter på NTNU som har faget programvareutvikling. Målet er at studentene skal lære hvordan
man kan jobbe sammen i et team for å lage et programvareprodukt, og hvordan vi kan bruke SCRUM-prosessen for å bistå til dette.

## Versjonsstatus
Build: Passing

## Kodestil
Codestyle: PEP8

## Skjermbilder
# ![Startpage](rodTraa/static/img/startpage.png)
# ![Feed](rodTraa/static/img/feed.png)

## Tech/rammeverk brukt

Programmet er skrevet i Python i rammeverket Django. Vi bruker MYSQL som backend database. Vi bruker også Pillow-biblioteket til bildehåndtering, 
Bootstrap-biblioteket til design og layout og applikasjonen Django Crispy Forms til forms.

## Egenskaper

Programmet vårt gjør det mulig for ulike mennesker som alle er interessert i strikking å holde hverandre oppdatert på hva de holder på med, har planer om 
og har gjort til nå. Det legger til rette for å dele innlegg med tekst og bilder, se andres innlegg, like innlegg og søke opp andres profiler for å se på dem.
Programmet legger også opp til kommunikasjon mellom enkeltpersoner og bedrifter, ved at bedrifter som har strikkere som målgruppe kan legge ut oppskrifter 
og arrangementer som strikkerne på plattformen kan se, bruke og delta på.

## Installering

Vi har brukt utviklingsverktøyet Pycharm, og kommer derfor til å vise hvordan utviklingsmiljøet kan settes opp i dette.
Programmet krever at følgende programmer er installert:

Python (Vi bruker versjon 3.8.1)

pip (for installasjon av flere av de resterende programmene)

Django (Installert ved hjelp av pip)

Django Crispy Forms (Vi installerte denne i Project Interpeteren i Pycharm)

Pillow (Installert gjennom Pycharm Project Interpreter)

MYSQLclient (Installert ved hjelp av pip, deretter installert gjennom Pycharm Project Interpreter)

Deretter kan koden klones fra GitLab, og en forbindelse mellom Pycharm og GitLab kan opprettes gjennom Pycharms innebygde git-verktøy.

Databaseinformasjon er spesifisert i settings-filen. Programmet er konfigurert til å bruke NTNU-databasen til Sander Høyland.

Vi har ikke fått til å installere og bruke MYSQLclient på alle PCene på gruppa, så vi har en branch med en lokal SQLite-database som
kan brukes hvis det oppstår problemer med MSQLclient. De to branchene skal være like med unntak av databasen. For å opprette SQLite-database må
følgende kodelinjer kjøres:
```
python manage.py makemigrations
```
og deretter
```
python manage.py migrate
```

For å kjøre programmet, kjører man følgende kodesnutt i den øverste RodTraa-mappen: 
```
python manage.py runserver
```
Deretter kan programmet åpnes i nettleseren på localhost:8000

## Tester

Det er skrevet enhetstester for å teste programmet. Se [oversikt over kodekvalitet](https://gitlab.stud.idi.ntnu.no/tdt4140-2020/58/-/wikis/Home/Oversikt-over-kodekvalitet) på wikien for mer utfyllende informasjon om testingen av prosjektet. Alle enhetstestestene i programmet kjøres ved:

```
python manage.py test
```

## Referanser

Vi har tatt mye inspirasjon fra YouTube-serien til Corey Schafer om Django (https://www.youtube.com/user/schafer5)
Vi har også brukt bøkene Software Engineering av Ian Sommerville og Scrum and XP from the Trenches til veiledning underveis i prosjektet
