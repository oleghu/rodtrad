from django.shortcuts import render
from ..feed.models import Post
from django.contrib.auth.models import User
from apps.feed.models import Challange, Event
from apps.users.models import PrivatePerson, Company


# Create your views here.
def index(request):
   return render(request, "index.html")

def startpage(request):
   posts = Post.objects.all().order_by('-time')
   return render(request, "startpage.html", {'posts':posts})

def myprofile(request,pk):
   knitter = User.objects.filter(pk=pk).first()
   user = User.objects.filter(pk = pk).first()
   posts = Post.objects.filter(post_owner= user)

   return render(request, "myprofile.html",{'knitter': knitter, 'posts': posts})

def companyprofile(request,pk):
   knitter = User.objects.filter(pk=pk).first()
   user = User.objects.filter(pk = pk).first()
   posts = Challange.objects.filter(post_owner=user)
   return render(request, "companyprofile.html",{'knitter': knitter, 'posts':posts})

def challenges(request):
   challengeposts = Challange.objects.all().order_by('-time')
   privateperson = False
   if PrivatePerson.objects.filter(user=request.user).first():
      privateperson = True
   return render(request, "challenges.html", {'challengeposts': challengeposts, 'privateperson': privateperson})

def editprofile(request):
   return render(request, "editprofile.html")

def events(request):
   eventposts = Event.objects.all().order_by('-time')
   user = request.user
   return render(request, "event.html",{'eventposts': eventposts, 'user':user})

