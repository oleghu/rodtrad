from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model

class Tests(TestCase):

        @classmethod
        def setUpTestData(self):
               self.user_signup_data = {
                        'username': 'Bob',
                        'email': 'bareTe@te.no',
                        'password1': 'blaabaerte11',
                        'password2': 'blaabaerte11'
                }


        def test_Urls(self):
                response = self.client.post(
                        reverse('register'),
                        self.user_signup_data,
                        follow=True
                )

                self.assertRedirects(response, reverse('login'))
                self.assertEqual(response.status_code, 200)
                self.assertTemplateUsed('login.html')


        def test_user(self):
                username = 'Lisa'
                user = get_user_model()
                User.objects.create_user(username, 'blaabarTe@te.no', 'password1')
                self.assertEquals(str(user.objects.get(username=username)), 'Lisa')
                self.assertTrue(user is not None and user.is_authenticated)
                self.assertEqual(User.objects.all().count(), 1)




