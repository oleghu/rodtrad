from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'apps.users'


class ProfilesConfig(AppConfig):
    name = 'profiles'


