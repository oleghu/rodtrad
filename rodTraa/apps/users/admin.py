from django.contrib import admin

# Register your models here.

from .models import PrivatePerson, Company

admin.site.register(PrivatePerson)
admin.site.register(Company)
