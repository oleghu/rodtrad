from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import PrivatePerson, Company

class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']


class EditPrivateProfile(UserChangeForm):
    name = forms.CharField(required=False,widget=forms.Textarea(attrs={'rows':1,'cols':40}))
    about = forms.CharField(required=False,widget=forms.Textarea(attrs={'rows':5,'cols':40}))

    class Meta:
        model= PrivatePerson
        fields = ('name', 'profilePicture','about')


class EditCompanyProfile(UserChangeForm):
    name = forms.CharField(required=False, widget=forms.Textarea(attrs={'rows': 1, 'cols': 40}))
    about = forms.CharField(required=False, widget=forms.Textarea(attrs={'rows': 5, 'cols': 40}))
    class Meta:
        model = Company
        fields = ('name','profilePicture','about')
