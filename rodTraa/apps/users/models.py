from django.db import models
from django.contrib.auth.models import User
# Create your models here.
# User= settings.AUTH_USER_MODEL
from django.http import request



class PrivatePerson(models.Model):
    user = models.ForeignKey(User, on_delete= models.CASCADE, null= False)
    name = models.TextField(null=True, blank=True, max_length=30)
    profilePicture = models.ImageField(upload_to='static.img/', null=True, blank=True)
    about = models.TextField(null=True, blank=True, max_length=400)


class Company(models.Model):
    user = models.ForeignKey(User, on_delete= models.CASCADE, null=False)
    name = models.TextField(null=True, blank=True, max_length=30)
    profilePicture = models.ImageField(upload_to='static.img/', null=True, blank=True)
    about = models.TextField(null=True, blank=True, max_length=400)

