from django.contrib import messages

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from .forms import UserRegisterForm
from django.contrib.auth import logout as django_logout, get_user_model
from .models import PrivatePerson, Company
from django.db.models import Q
from django.shortcuts import render, get_object_or_404, redirect
from ..staticpages.views import myprofile
from .forms import UserRegisterForm, EditPrivateProfile, EditCompanyProfile
from django.contrib.auth.models import User
from django.urls import reverse
from django.contrib.auth.forms import UserChangeForm
from apps.feed.models import Post, Challange

User = get_user_model()
USERS_PER_PAGE = 10


def logout(request):
    django_logout(request)
    return redirect('index')


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        is_company = request.POST.get('company')
        if form.is_valid():
            username = form.cleaned_data.get('username')
            messages.success(request, f'Account created for {username}!')
            usero = form.save()
            if(is_company):
                company = Company.objects.create(user=usero)
            else:
                privateperson = PrivatePerson.objects.create(user=usero)
            return redirect('login')
    else:
        form = UserRegisterForm()
    return render(request, 'register.html', {'form': form})


def redirect_to_index(request):
    return redirect('startpage')

def search_users(request):
    context = {}
    query = ""
    if 'q' in request.GET:
        query = request.GET['q']
        context['query'] = str(query)
    users = get_blog_queryset(query)
    page = request.GET.get('page', 1)
    blog_posts_paginator = Paginator(users, USERS_PER_PAGE)
    try:
        users = blog_posts_paginator.page(page)
    except PageNotAnInteger:
        users = blog_posts_paginator.page(USERS_PER_PAGE)
    except EmptyPage:
        users = blog_posts_paginator.page(blog_posts_paginator.num_pages)

    context = {'users': users}
    return render(request, 'search_list.html', context)


def get_blog_queryset(query=None):
    queryset = []
    queries = query.split(" ")
    for q in queries:
        users = User.objects.filter(username__contains=q)
        for user in users:
            queryset.append(user)
    return list(set(queryset))


def view_profile(request,pk=None):
    personalkey = pk;
    if pk:
        user= User.objects.get(pk=pk)
    else:
        user = request.user
    posts = Post.objects.filter(post_owner = user).order_by('-time')
    challenge = Challange.objects.filter(post_owner=user).order_by('-time')

    is_privateperson = False
    challenges = []
    if PrivatePerson.objects.filter(user=user).first():
        profile = PrivatePerson.objects.filter(user=user).first()
        is_privateperson = True
        challenges = Challange.objects.filter(alignChallange__id__exact=profile.id)
        return render(request, 'myprofile.html',{'profile':profile,'posts':posts, 'is_privatperson': is_privateperson, 'challenges':challenges,'personalkey':personalkey})
    else:
        profile = Company.objects.filter(user=user).first()
        return render(request, 'companyprofile.html',{'profile':profile,'posts':posts, 'challenges':challenges, 'challenge':challenge,'personalkey':personalkey})

def editprofile(request):
    if PrivatePerson.objects.filter(user=request.user).first():
        if request.method == 'POST':
            privateperson = PrivatePerson.objects.filter(user = request.user).first()
            form = EditPrivateProfile(request.POST, request.FILES, instance=privateperson)
            if form.is_valid():
                form.save()
                return redirect('myprofile', request.user.pk)
        else:
            privateperson = PrivatePerson.objects.filter(user=request.user).first()
            form = EditPrivateProfile(instance=privateperson)
            is_privateperson = True
            return render(request, 'editprofile.html', {'form': form, 'is_privateperson':is_privateperson})
    else:
        if request.method == 'POST':
            company = Company.objects.filter(user = request.user).first()
            form = EditCompanyProfile(request.POST, request.FILES, instance= company)
            if form.is_valid():
                form.save()
                return redirect('myprofile', request.user.pk)
        else:
            company = Company.objects.filter(user=request.user).first()
            form = EditCompanyProfile(instance= company)
            is_privateperson = False
            return render(request, 'editprofile.html', {'form': form, 'is_privateperson':is_privateperson})

