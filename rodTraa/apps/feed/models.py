from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from apps.users.models import PrivatePerson


# Create your models here.


class Post(models.Model):
    text = models.TextField(null=True, blank=True,max_length=4000)
    image = models.ImageField(null=False)
    time = models.DateTimeField(null=True)
    post_owner = models.ForeignKey(User, on_delete= models.CASCADE, null=True)
    like = models.ManyToManyField(User, blank=True, related_name="like")

class Challange(models.Model):
    name = models.TextField(null=True, blank = True, max_length=40)
    text = models.TextField(null=True, blank=True, max_length=4000)
    image = models.ImageField(null=False)
    time = models.DateTimeField(null=True)
    post_owner = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    alignChallange = models.ManyToManyField(PrivatePerson, null=True, blank = True)

class Event(models.Model):
    text = models.TextField(null=False, blank = False, max_length=4000)
    time = models.DateTimeField(null=True)
    post_owner = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    interested = models.ManyToManyField(User, blank=True, related_name="interested")
