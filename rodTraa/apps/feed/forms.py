from django import forms
from .models import Post, Challange, Event


class PostForm(forms.ModelForm):
    text = forms.CharField(required=False, widget=forms.Textarea(attrs={'rows': 5, 'cols': 40}))

    class Meta:
        model = Post
        fields = ("text", "image")
        exclude = ("time", "post_owner")
        


class ChallengeForm(forms.ModelForm):
    name = forms.CharField(required=False, widget=forms.Textarea(attrs={'rows': 1, 'cols': 40}))
    text = forms.CharField(required=False, widget=forms.Textarea(attrs={'rows': 5, 'cols': 40}))

    class Meta:
        model = Challange
        fields = ("name","text", "image")
        exclude = ("time", "post_owner", "alignChallange")

class EventForm(forms.ModelForm):
    text = forms.CharField(required=True, widget= forms.Textarea(attrs={'rows':5, 'cols':50, 'placeholder':'Skriv litt om arrangementet ditt'}))

    class Meta:
        model = Event
        fields = ("text",)
        exclude = ("time", "post_owner")