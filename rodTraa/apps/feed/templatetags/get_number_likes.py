from django import template
from apps.feed.models import Post
register = template.Library()

@register.filter(name='get_number_likes')
def get_number_likes(pk):
    return Post.objects.filter(pk=pk).first().like.all().count()