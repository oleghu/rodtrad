from django import template
from apps.feed.models import Event

register = template.Library()

@register.filter(name='get_if_interested')
def get_if_interested(value, pk):
    if Event.objects.filter(pk=pk).first().interested.filter(username=value).first():
        return True
    else:
        return False