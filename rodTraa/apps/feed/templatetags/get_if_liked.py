from django import template
from apps.feed.models import Post

register = template.Library()

@register.filter(name='get_if_liked')
def get_if_liked(value, pk):
    if Post.objects.filter(pk=pk).first().like.filter(username=value).first():
        return True
    else:
        return False