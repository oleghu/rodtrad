from django import template
from apps.feed.models import Event
register = template.Library()

@register.filter(name='get_number_interested')
def get_number_interested(pk):
    return Event.objects.filter(pk=pk).first().interested.all().count()