from django import template
from apps.feed.models import Challange

register = template.Library()

@register.filter(name='alignChallange')
def alignChallange(value, pk):
    if Challange.objects.filter(pk=pk).first().alignChallange.filter(user=value).first():
        return True

