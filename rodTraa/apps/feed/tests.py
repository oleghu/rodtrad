from django.contrib.auth import get_user_model
from django.test import TestCase
from django.contrib.auth.models import User
from .models import Post


class TestUrls(TestCase):

    def test_numberOfPosts(self):
        username = 'Lisa'
        user = get_user_model()
        users = User.objects.create_user('Lisa', 'blaabarTe@te.no', 'password1')
        fm = Post(text='blblblb', image='', post_owner=users)
        self.assertEquals(str(fm.post_owner), str(user.objects.get(username=username)))

