from django.shortcuts import render, redirect
from .forms import PostForm, ChallengeForm, EventForm
from django.utils import timezone
from apps.users.models import PrivatePerson, Company
from .models import Post, Event, Challange


# Create your views here.

def postpage(request):
    privateperson = False;
    if PrivatePerson.objects.filter(user=request.user).first():
        privateperson = True;
        if request.method == 'POST':
            form = PostForm(request.POST, request.FILES)
            form.instance.post_owner = request.user
            form.instance.time = timezone.now()

            if form.is_valid():
                form.save()
                return redirect('startpage')
        else:
            form = PostForm()
        return render(request, 'postpage.html', {'form': form, 'privateperson':privateperson})

    else:
        if request.method == 'POST':
            form = ChallengeForm(request.POST, request.FILES )
            form.instance.post_owner = request.user
            form.instance.time = timezone.now()

            if form.is_valid():
                form.save()
                return redirect('challenges')
        else:
            form = ChallengeForm()
        return render(request, 'postpage.html', {'form': form, 'privateperson':privateperson})

def deletePost(request,pk):
    post= Post.objects.filter(pk=pk).first()
    post.delete()
    return redirect('myprofile', request.user.pk)

def newevent(request):
    if request.method == 'POST':
        form = EventForm(request.POST)
        form.instance.post_owner = request.user
        form.instance.time = timezone.now()

        if form.is_valid():
            form.save()
            return redirect('events')
    else:
        form = EventForm()
    return render(request, 'newevent.html', {'form': form })

def interested(request, pk):
    event = Event.objects.filter(pk=pk).first()
    event.interested.add(request.user)
    return redirect('events')

def notInterested(request,pk):
    event = Event.objects.filter(pk=pk).first()
    event.interested.remove(request.user)
    return redirect('events')

def like(request, pk):
    post = Post.objects.filter(pk=pk).first()
    post.like.add(request.user)
    return redirect('startpage')

def alignChallange(request,pk):
    challange = Challange.objects.filter(pk=pk).first()
    privatePerson = PrivatePerson.objects.filter(user=request.user).first()
    if privatePerson:
        challange.alignChallange.add(privatePerson)
    return redirect('challenges')

