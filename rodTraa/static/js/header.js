var html = [

 '<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top" id="navbar">',
        '<a class="navbar-brand" href="#" id="logo">',
            '<img href="" src="../../static/Frame1.png"/>',
        '</a>',
     '<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">',
    '<span class="navbar-toggler-icon"></span>',
  '</button>',

     '<div class="collapse navbar-collapse" id="navbarNav">',

        '<ul class="navbar-nav">',
          '<li class="nav-item">',
            '<a class="nav-link" href="#"> Min profil</a>',
          '</li>',

         '<li class="nav-item">',
            '<a class="nav-link" href="#"> Nytt inlegg </a>',
          '</li>',

         '<li class="nav-item">',
            '<a class="nav-link" href="#"> Utfordringer <span class="sr-only"> </span></a>',
          '</li>',
        '</ul>',


      '<form class="form-inline" id="search">',
      '<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" id="seachInput">',
      '<button class="btn btn-outline-success my-2 my-sm-0 text-black-50" id="searchButton" type="submit" >Search</button>',
    '</form>',
     '</div>',

          '<ul class="navbar-nav ml-auto">',
       '<li class="nav-item">',
        '<a class="nav-link" href="#"> Logg ut </a>',
      '</li>',
      '</ul>',

    '</nav>',


].join('');

var div = document.createElement('div');
    div.innerHTML = html;

document.getElementById('navbar').appendChild(div);

