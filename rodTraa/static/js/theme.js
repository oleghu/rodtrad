changeTheme()
function changeTheme(){

    let theme = localStorage.getItem('theme')
    theme = theme ? theme : 'light';
    let elements = document.body.querySelectorAll("*")
    console.log(theme)

    elements.forEach(elem => {
        let classes = elem.className
        classes = classes.replace(/dark/g, theme)
        classes = classes.replace(/light/g, theme)

        elem.className = classes
        document.body.className = 'body-'+theme;
    })
}
