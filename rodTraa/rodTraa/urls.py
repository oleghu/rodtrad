"""rodTraa URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('startpage/', include('startpage.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static

from . import settings
from apps.staticpages.views import index, startpage, myprofile
from apps.users import views as user_views
from apps.staticpages.views import challenges, events
from apps.feed.views import postpage, deletePost, newevent, interested, notInterested, like, alignChallange
from apps.users.views import editprofile
from django.contrib.auth import views as auth_views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('register', user_views.register, name='register'),
    path('', index, name='index'),
    path('startpage/', startpage, name='startpage'),
    path('myprofile/<int:pk>/', user_views.view_profile, name='myprofile'),
    path('companyprofile/<int:pk>/', user_views.view_profile, name='companyprofile'),
    path('accounts/', include('django.contrib.auth.urls'), name="login"),
    path('accounts/profile/', user_views.redirect_to_index, name="profile"),
    path('challenges/', challenges, name="challenges"),
    path('list/', user_views.search_users, name="search_results"),
    path('postpage/', postpage, name= "postpage"),
    path('editprofile/', editprofile, name = "editprofile"),
    path('deletePost/<int:pk>', deletePost, name = "deletepost"),
    path('events/', events, name="events"),
    path('newevent/', newevent, name="newevent"),
    path('interested/<int:pk>/', interested, name="interested"),
    path('notInterested/<int:pk>/',notInterested, name="notInterested"),
    path('like/<int:pk>/', like, name="like"),
    path('alignChallange/<int:pk>/', alignChallange, name="alignChallange"),
    path('password-reset/',
         auth_views.PasswordResetView.as_view(template_name='password_reset.html'),
         name="password_reset"),
    path('password-reset/done/',
         auth_views.PasswordResetDoneView.as_view(template_name='password_reset_done.html'),
         name="password_reset_done"),
    path('password-reset-confirm/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(template_name='password_reset_confirm.html'),
         name="password_reset_confirm"),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


